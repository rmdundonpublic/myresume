<?php
require_once '../vendor/autoload.php';

$resume = json_decode(file_get_contents('resume.json'));
$template = file_get_contents('resume.mustache');


// Will separate the header, or something
$header = <<<EOS
<!doctype html>
<html>
<head>
	<title>{$resume->basics->name}'s Resume</title>
	<link href="resume.css" rel="stylesheet" />
</head>
EOS;
echo $header;

$mustache = new Mustache_Engine;
echo $mustache->render($template, $resume);
